import random
from os import name

b = []


def generate_bunches():
    global b
    b = []
    for _ in range(3):
        a = random.randint(1, 10)
        b.append(a)


def get_bunches():
    print(b)


def get_from_bunch(name, qua):
    if qua > b[name - 1]:
        print('Ты взял больше чем есть в кучке, бери меньше!')
    else:
        b[name - 1] -= qua


def rule():
    rul = random.randint(1, 5)
    print('In this game you can take only', rul, 'balls.')


def control_rule(a, b):
    if a != b:
        print('Follow the rule!')


def game_over():
    s = sum(b)
    return s
